<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bigspace
 */


get_template_part( 'part', 'footer' );

?>


</div>
<style> #m-nav {
        padding-top: 22px;
        padding-right: 5px;
    }
</style>

<script type="text/javascript" src="<?php echo get_template_directory_uri() . '/assets/forkit.js.download'?>"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var pollsL10n = {
        "ajax_url": "https:\/\/www.bigdropinc.com\/wp-admin\/admin-ajax.php",
        "text_wait": "Your last request is still being processed. Please wait a while ...",
        "text_valid": "Please choose a valid poll answer.",
        "text_multiple": "Maximum number of choices allowed: ",
        "show_loading": "1",
        "show_fading": "1"
    };
    /* ]]> */
</script>
<script>
    var rangeOpts = {
        from: 3,
        values: [5000, 10000, 15000, 20000, 25000, 30000, 35000, 40000, 45000, 50000, 60000, 70000, 80000, 90000, 100000, 150000, 200000, 250000, 300000, 350000, 400000, 450000, 500000]
    };
    $("#range").ionRangeSlider({
        hide_min_max: true,
        hide_from_to: true,
        grid: false,
        values: rangeOpts.values,
        from: rangeOpts.from,
        onStart: function (data) {
            var val = data.from_value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            $('.range-result span').text(val);
        },
        onChange: function (data) {
            var val = data.from_value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            $('.range-result span').text(val);
        },
    });
</script>
<script>
    var new_form = $(document).find('form#new-request-form');
    new_form.on('click', '.submit-step', function (e) {
        e.preventDefault();
        myFunction1();
        var serialize_form = new_form.serialize();
        var data = serialize_form;
        $.ajax({
            url: 'php/form.php',
            data: data,
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
            },
            success: function (res) {
                console.log(res);
                switch (res.status) {
                    case 'validate_error':
                        new_form.find('.form-error').text(res.msg).show().fadeOut(4000);
                        var names = [];
                        $.each(res.errors, function (key, value) {
                            names.push(value);
                        });
                        if (names.length > 0) {
                            new_form.find('.parsley-error').each(function () {
                                $(this).removeClass('parsley-error');
                            });
                            $.each(names, function (index, value) {
                                new_form.find("[name='" + value + "']").addClass('parsley-error');
                            });
                        }
                        break;
                    case 'captcha_error':
                        new_form.find('.form-error').text(res.msg).show().fadeOut(4000);
                        break;
                    case 'step1_is_valid':
                        new_form.find('.parsley-error').each(function () {
                            $(this).removeClass('parsley-error');
                        });
                        new_form.find('#step1').hide();
                        new_form.find('#step2').show();
                        new_form.find('#stepLocator').val(2);

                        break;
                    case 'success':
                        new_form.find('.parsley-error').each(function () {
                            $(this).removeClass('parsley-error');
                        });
                        new_form.find('.form-error').text(res.msg).show();
                        setTimeout(function () {
                            document.location.href = res.redirect_url;
                        }, 1500);
                        break;
                }
                return false;
            }
        });
        return false;
    });
    new_form.on('click', '.prev-step', function (e) {
        e.preventDefault();
        new_form.find('fieldset#step2').hide();
        new_form.find('fieldset#step1').show();
        new_form.find('#stepLocator').val(1);
    });

    // window.onscroll = () => {
    //   if(this.scrollY <= window.innerHeight) {
    //     $('#imageBig').show();
    //     console.log("showing");
    //   }
    //     else {
    //       console.log("hidina");
    //       $('#imageBig').css({
    //         'display': 'none'
    //     });
    //     }
    //   };
</script>

<?php wp_footer(); ?>

</body>
</html>

