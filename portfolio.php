<?php
/*
Template Name: Portfolio page
*/

get_header('portfolio');
?>
      <div id="content" class="">
         <div id="works-wrap">
            <div id="works">
                <?php

                $data = carbon_get_post_meta( $post->ID, 'crb_places', 'complex' );

                if ( ! empty( $data ) ): ?>

                        <?php foreach ( $data as $tr ): ?>
                        <a class="work  <?php echo 'work'.$i?>" href="<?php echo $tr['link'] ?>" name="zeromasswater" data-type="" data-url="<?php echo $tr['link'] ?>" data-id="1">
                            <div class="work-overlay"></div>
                            <img alt="Zero Mass Water Web Design Preview" src="<?php echo $slide['photo'] ?>" />
                        </a>

                        <?php
                    endforeach; ?>

                <?php endif; ?>


            </div>
         </div>
      </div>
<?php
get_footer();

?>
<script>
   function pTop() {
       var navHeight = $('.work-category-nav').innerHeight();
       var headerHeight = $('#header').innerHeight();
       $('#works-wrap').css('padding-top', headerHeight + 'px');
       if($(window).width() > 1024) {
           $('#works').css('padding-top', navHeight + 'px');
       }else{
           $('#works').css('padding-top', 0);
       }
   }
   pTop();
   $(window).on('resize', function () {
       pTop();
   });
</script>
