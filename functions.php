<?php
/**
 * bigspace functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package bigspace
 */

if (!function_exists('bigspace_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function bigspace_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on bigspace, use a find and replace
         * to change 'bigspace' to the name of your theme in all the template files.
         */
        load_theme_textdomain('bigspace', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'menu-1' => esc_html__('Primary', 'bigspace'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('bigspace_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array(
            'height' => 250,
            'width' => 250,
            'flex-width' => true,
            'flex-height' => true,
        ));
    }
endif;
add_action('after_setup_theme', 'bigspace_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function bigspace_content_width()
{
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('bigspace_content_width', 640);
}

add_action('after_setup_theme', 'bigspace_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function bigspace_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'bigspace'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'bigspace'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'bigspace_widgets_init');

/**
 * Enqueue scripts and styles.
 */


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
    require get_template_directory() . '/inc/jetpack.php';
}



/*Bigspace css and script */
add_filter('navigation_markup_template', 'my_navigation_template', 10, 2 );
function my_navigation_template( $template, $class ){
    /*
    Вид базового шаблона:
    <nav class="navigation %1$s" role="navigation">
        <h2 class="screen-reader-text">%2$s</h2>
        <div class="nav-links">%3$s</div>
    </nav>
    */

    return '
	<div class="pagination-wrap %1$s" role="navigation">
		<div class="nav-links wp-pagenavi">%3$s</div>
	</div>    
	';
}

function bigspace_scripts_c()
{

    wp_enqueue_style('bigspace-style', get_stylesheet_uri());
    wp_enqueue_style('blog-style', get_template_directory_uri() . '/assets/blog.css');
    wp_enqueue_style('contact-style', get_template_directory_uri() . '/assets/contact.css');
    wp_enqueue_style('contact-page-style', get_template_directory_uri() . '/assets/contact-page.css');
    wp_enqueue_style('global-style', get_template_directory_uri() . '/assets/global.css');
    wp_enqueue_style('new-request-form-style', get_template_directory_uri() . '/assets/new-request-form.css');
    wp_enqueue_style('polls-css-style', get_template_directory_uri() . '/assets/polls-css.css');
    wp_enqueue_style('services-style', get_template_directory_uri() . '/assets/services.css');
    wp_enqueue_style('styles-style', get_template_directory_uri() . '/assets/styles.css');


    wp_enqueue_script('modernizr.custom.js.download-js', get_template_directory_uri() . '/assets/modernizr.custom.js.download', array("jquery"));
    wp_enqueue_script('jquery.js.download-js', get_template_directory_uri() . '/assets/jquery.js.download', array("jquery"));
    wp_enqueue_script('jquery-migrate.min.js.download-js', get_template_directory_uri() . '/assets/jquery-migrate.min.js.download', array("jquery"));
    wp_enqueue_script('front.js.download-js', get_template_directory_uri() . '/assets/front.js.download', array("jquery"));
    wp_enqueue_script('6897.js.download-js', get_template_directory_uri() . '/assets/6897.js.download', array("jquery"));
    wp_enqueue_script('sharethis.js.download-js', get_template_directory_uri() . '/assets/sharethis.js.download', array("jquery"));


    wp_enqueue_script('scripts.js.download-js', get_template_directory_uri() . '/assets/scripts.js.download', array("jquery"), true);

    wp_enqueue_script('forkit.js.download-js', get_template_directory_uri() . '/assets/forkit.js.download', array("jquery"), true);
    wp_enqueue_script('polls-js.js.download-js', get_template_directory_uri() . '/assets/polls-js.js.download', array("jquery"), true);
    wp_enqueue_script('production.js.download-js', get_template_directory_uri() . '/assets/production.js.download', array("jquery"), true);
    wp_enqueue_script('wp-embed.min.js.download-js', get_template_directory_uri() . '/assets/wp-embed.min.js.download', array("jquery"), true);


}

add_action('wp_enqueue_scripts', 'bigspace_scripts_c');


/* Carbon fields*/


use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('carbon_fields_register_fields', 'crb_attach_theme_options'); // Для версии 2.0 и выше
//add_action( 'carbon_register_fields', 'crb_attach_theme_options' ); // Для версии 1.6 и ниже

add_action('after_setup_theme', 'crb_load');
function crb_load()
{
    require_once('carbon-fields/vendor/autoload.php');
    \Carbon_Fields\Carbon_Fields::boot();
}

function crb_attach_theme_options()
{

    Container::make('theme_options', 'Main options')
        ->add_fields(array(
            Field::make('text', 'crb_facebook_url')->set_width(50),
            Field::make('text', 'crb_twwiter_url')->set_width(50),
            Field::make('text', 'crb_in_url')->set_width(50),
            Field::make('text', 'crb_google_url')->set_width(50),
            Field::make('text', 'crb_instagram_url')->set_width(50),
            Field::make('text', 'crb_pinterest_url')->set_width(50),
            Field::make('text', 'crb_beha_url')->set_width(50),

            Field::make('text', 'crb_email_protected')->set_width(50),


            Field::make('text', 'crb_privacy_policy_url')->set_width(50),
            Field::make('text', 'crb_terms_conditions_url')->set_width(50),

            Field::make("rich_text", "crb_footer_text", "Footer text")
                ->help_text('Write text for footer'),


            Field::make('text', 'crb_ecommerce_url', 'eCommerce')->set_width(33),
            Field::make('text', 'crb_application_url', 'Application')->set_width(33),
            Field::make('text', 'crb_wp_url', 'WordPress')->set_width(33),


        ));

    Container::make('post_meta', 'Main info')
        ->show_on_post_type('page')
        ->show_on_template('sub-services.php')
        ->add_fields(array(
            Field::make("image", "photo", "Main photo")->set_value_type('url'),


        ));

    Container::make('post_meta', 'Main info')
        ->show_on_post_type('page')
        ->show_on_template('about.php')
        ->add_fields(array(
            Field::make("text", "crb_first_text", "First text")->set_width(50),
            Field::make("image", "photo1", "First photo")->set_value_type('url')->set_width(50),

            Field::make("text", "crb_second_text", "Second text")->set_width(50),
            Field::make("image", "photo2", "Second photo")->set_value_type('url')->set_width(50),

            Field::make("text", "crb_third_text", "Third text")->set_width(50),
            Field::make("image", "photo3", "Third photo")->set_value_type('url')->set_width(50),

            Field::make("text", "crb_fourth_text", "Fourth text")->set_width(50),
            Field::make("image", "photo4", "Fourth photo")->set_value_type('url')->set_width(50),

            Field::make('text', 'crb_su_url', 'Susisiekite'),

        ));


    Container::make('post_meta', 'Main info')
        ->show_on_post_type('page')
        ->show_on_template('main.php')
        ->add_fields(array(
            Field::make("text", "crb_portfolio_url", "Button link")->set_width(50),
            Field::make("text", "crb_text", "Button text")->set_width(50),


        ));

    Container::make('post_meta', 'Main info')
        ->show_on_post_type('page')
        ->show_on_template('services.php')
        ->add_fields(array(
            Field::make("text", "crb_1_url", "Button link")->set_width(50),
            Field::make("text", "crb_text1", "Button text")->set_width(50),


            Field::make("text", "crb_2_url", "Button link")->set_width(50),
            Field::make("text", "crb_text2", "Button text")->set_width(50),


            Field::make("text", "crb_3_url", "Button link")->set_width(50),
            Field::make("text", "crb_text3", "Button text")->set_width(50),


            Field::make("text", "crb_4_url", "Button link")->set_width(50),
            Field::make("text", "crb_text4", "Button text")->set_width(50),


            Field::make("text", "crb_5_url", "Button link")->set_width(50),
            Field::make("text", "crb_text5", "Button text")->set_width(50),


            Field::make("text", "crb_6_url", "Button link")->set_width(50),
            Field::make("text", "crb_text6", "Button text")->set_width(50),


            Field::make("text", "crb_7_url", "Button link")->set_width(50),
            Field::make("text", "crb_text7", "Button text")->set_width(50),


        ));


    Container::make('post_meta', 'Main info')
        ->show_on_post_type('page')
        ->show_on_template('contact.php')
        ->add_fields(array(
            Field::make("text", "crb_2_title", "Second screen title"),
            Field::make("rich_text", "crb_text1", "First adress block"),
            Field::make("rich_text", "crb_text2", "Second adress block"),
            Field::make("rich_text", "crb_text3", "Third adress block"),


            Field::make("text", "crb_3_title", "Third screen title"),
            Field::make("rich_text", "crb_text11", "First adress block"),
            Field::make("rich_text", "crb_text22", "Second adress block"),
            Field::make("rich_text", "crb_text33", "Third adress block"),

        ));


    Container::make('post_meta', 'Main info')
        ->show_on_post_type('page')
        ->show_on_template('portfolio.php')
        ->add_fields(array(


            Field::make('complex', 'crb_places', 'Portfolio')
                ->add_fields(array(
                        Field::make('text', 'link', 'Link')->set_width(50),
                        Field::make("image", "photo", "Photo")->set_value_type('url')->set_width(50),)
                ),
        ));


}




