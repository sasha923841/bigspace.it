<?php
/*
Template Name: Services page
*/

get_header('services');
?>
      <div id="content" class="">
         <main class="service-index services">
            <div class="wrap">
               <div class="container">
                  <div class="services-intro">
                      <?php while( have_posts() ) : the_post();

                          the_content(); // выводим контент
                      endwhile; ?>
                  </div>
                  <div class="services-list">
                     <a href="web-design.html" class="item with-shadow" style="background-image: url(<?php echo get_template_directory_uri() . '/assets/images/web-design2.jpg'?>)">
                        <h2>Puslapių dizainas</h2>
                     </a>
                     <a href="web-dev.html" class="item with-shadow" style="background-image: url(<?php echo get_template_directory_uri() . '/assets/images/web-dev.jpg'?>)">
                        <h2>Svetainių kūrimas</h2>
                     </a>
                     <a href="starters.html" class="item with-shadow" style="background-image: url(<?php echo get_template_directory_uri() . '/assets/images/startup.jpg'?>)">
                        <h2>Startuoliams</h2>
                     </a>
                     <a href="branding.html" class="item with-shadow" style="background-image: url(<?php echo get_template_directory_uri() . '/assets/images/branding.jpg'?>)">
                        <h2>Prekės ženklinimas</h2>
                     </a>
                     <a href="saas-rent.html" class="item with-shadow" style="background-image: url(<?php echo get_template_directory_uri() . '/assets/images/saas-rent.jpg'?>)">
                        <h2>SAAS - programinės įrangos nuomos paslauga</h2>
                     </a>
                     <a href="hosting.html" class="item with-shadow" style="background-image: url(<?php echo get_template_directory_uri() . '/assets/images/hosting.jpg'?>)">
                        <h2>(Hosting) Interneto svetainių palaikymas ir talpinimas</h2>
                     </a>
                     <a href="advertising.html" class="item with-shadow" style="background-image: url(<?php echo get_template_directory_uri() . '/assets/images/advertisment.jpg'?>)">
                        <h2>Skaitmeninės reklamos kūrimas</h2>
                     </a>
                  </div>
               </div>
               <div class="request-block text-center">
                  <div class="container">
                     <h2 class="h1 textbig">Sukurkime ką nors kartu.</h2>
                     <a href="#" class="btn btn-white request-quote btn-request-quote">Užpildyti užklausą</a>
                  </div>
               </div>
            </div>
         </main>
      </div>
<?php get_footer(); ?>
