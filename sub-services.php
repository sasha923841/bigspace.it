<?php
/*
Template Name: Sub services
*/

get_header('sub-services');
?>

    <div class="container-lg">

        <div class="featured-img" style="background-image: url(<?php echo carbon_get_post_meta(get_the_ID(), 'photo');?>);"></div>
    </div>
    <div id="content" class="">
        <main class="service-single services">
            <div class="wrap">
                <div class="container">
                    <div class="services-intro">
                        <?php while( have_posts() ) : the_post();

                            the_content(); // выводим контент
                        endwhile; ?>
                    </div>
                </div>
                <div class="categories-list">
                    <div class="container">
                        <ul>
                            <li><a href="<?php echo carbon_get_theme_option('crb_ecommerce_url'); ?>">eCommerce</a></li>
                            <li><a href="<?php echo carbon_get_theme_option('crb_application_url'); ?>">Applications</a></li>
                            <li>
                                <a href="<?php echo carbon_get_theme_option('crb_wp_url'); ?>">Wordpress</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="request-block text-center">
                    <div class="container">
                        <h2 class="h1 textbig">Sukurkime ką nors kartu.</h2>
                        <a href="#" class="btn btn-white request-quote btn-request-quote">Užpildyti užklausą</a>
                    </div>
                </div>
            </div>
        </main>
    </div>
<?php
get_footer('sub-services');
