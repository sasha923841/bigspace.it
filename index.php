<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bigspace
 */

get_header('services');
?>

    <div id="content" class="">
        <main id="blogArchive">
            <section class="wrap">
                <div class="page-intro">
                    <strong class="title has-animate animated">Blog</strong>
                </div>
                <div class="big-gallery">
                    <div class="container">
                        <div class="slider animated dots-centered">
                            <div class="item">
                                <div class="image-holder"
                                     style="background-image: url('https://www.bigdropinc.com/wp-content/uploads/2018/04/How-a-Strong-Brand-Culture-Makes-Your-Company-More-Competitive-type-pdf-1920x1080.jpg')"></div>
                            </div>
                            <div class="item">
                                <div class="image-holder"
                                     style="background-image: url('https://www.bigdropinc.com/wp-content/uploads/2018/03/Digital-Advertising-Trends-1920x1080.jpg')"></div>
                            </div>
                            <div class="item">
                                <div class="image-holder"
                                     style="background-image: url('https://www.bigdropinc.com/wp-content/uploads/2018/03/Is-Voice-Marketing-the-Next-Big-Thing-for-Marketing_-02-1920x1080.png')"></div>
                            </div>
                            <div class="item">
                                <div class="image-holder"
                                     style="background-image: url('https://www.bigdropinc.com/wp-content/uploads/2018/02/Branding-v.-Marketing-A-Guide-01-1920x1070.png')"></div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="big info animated">
                            <div class="frame show">
                                <strong class="category">Creative and Branding</strong>
                                <h3>
                                    <a href="https://www.bigdropinc.com/blog/how-strong-brand-culture-makes-your-company-more-competitive/">How
                                        a Strong Brand Culture Makes your Company More Competitive</a></h3>
                                <div class="add-info">
                                    <div class="published">
                                        Published on
                                        <time>April 4th, 2018</time>
                                    </div>
                                    <div class="author">
                                        Written by
                                        <strong>Michelle Polizzi
                                        </strong>
                                    </div>
                                </div>
                            </div>
                            <div class="frame">
                                <strong class="category">Digital Marketing</strong>
                                <h3><a href="https://www.bigdropinc.com/blog/digital-advertising-trends-in-2018/">Digital
                                        Advertising Trends in 2018: 5 Things to Watch</a></h3>
                                <div class="add-info">
                                    <div class="published">
                                        Published on
                                        <time>March 29th, 2018</time>
                                    </div>
                                    <div class="author">
                                        Written by
                                        <strong>Beth Osborne
                                        </strong>
                                    </div>
                                </div>
                            </div>
                            <div class="frame">
                                <strong class="category">Digital Marketing</strong>
                                <h3><a href="https://www.bigdropinc.com/blog/voice-search-for-digital-marketing/">Is
                                        Voice Search the Next Big Thing for Digital Marketing?</a></h3>
                                <div class="add-info">
                                    <div class="published">
                                        Published on
                                        <time>March 22nd, 2018</time>
                                    </div>
                                    <div class="author">
                                        Written by
                                        <strong>Stephanie Knapp
                                        </strong>
                                    </div>
                                </div>
                            </div>
                            <div class="frame">
                                <strong class="category">Creative and Branding</strong>
                                <strong class="category">Digital Marketing</strong>
                                <h3><a href="https://www.bigdropinc.com/blog/branding-vs-marketing/">Branding vs
                                        Marketing: A Guide</a></h3>
                                <div class="add-info">
                                    <div class="published">
                                        Published on
                                        <time>March 20th, 2018</time>
                                    </div>
                                    <div class="author">
                                        Written by
                                        <strong>Michelle Polizzi
                                        </strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <ul id="archiveNav" class="text-center">
                    <?php

                    $args = array(
                        'show_option_all'    => '',
                        'show_option_none'   => __(''),
                        'orderby'            => 'name',
                        'order'              => 'ASC',
                        'show_last_update'   => 0,
                        'style'              => 'list',
                        'show_count'         => 0,
                        'hide_empty'         => 1,
                        'use_desc_for_title' => 1,
                        'child_of'           => 0,
                        'feed'               => '',
                        'feed_type'          => '',
                        'feed_image'         => '',
                        'exclude'            => '',
                        'exclude_tree'       => '',
                        'include'            => '',
                        'hierarchical'       => true,
                        'title_li'           => __( '' ),
                        'number'             => NULL,
                        'echo'               => 1,
                        'depth'              => 0,
                        'current_category'   => 0,
                        'pad_counts'         => 0,
                        'taxonomy'           => 'category',
                        'walker'             => 'Walker_Category',
                        'hide_title_if_empty' => false,
                        'separator'          => '',
                    );

                    wp_list_categories( $args ); ?>
                </ul>
                <ul id="archiveNav" class="text-center">
                    <li class="active">
                        <a href="https://www.bigdropinc.com/blog/" data-term="all">All</a>
                    </li>
                    <li class="">
                        <a href="https://www.bigdropinc.com/blog-categories/creative-branding/"
                           data-term="creative-branding">Creative and Branding</a>
                    </li>
                    <li class="">
                        <a href="https://www.bigdropinc.com/blog-categories/digital-marketing/"
                           data-term="digital-marketing">Digital Marketing</a>
                    </li>
                    <li class="">
                        <a href="https://www.bigdropinc.com/blog-categories/new-marketers/" data-term="new-marketers">New
                            Marketers</a>
                    </li>
                    <li class="">
                        <a href="https://www.bigdropinc.com/blog-categories/nonprofits/" data-term="nonprofits">Nonprofits</a>
                    </li>
                    <li class="">
                        <a href="https://www.bigdropinc.com/blog-categories/saas/" data-term="saas">SaaS</a>
                    </li>
                    <li class="">
                        <a href="https://www.bigdropinc.com/blog-categories/sem/" data-term="sem">SEM</a>
                    </li>
                    <li class="">
                        <a href="https://www.bigdropinc.com/blog-categories/seo/" data-term="seo">SEO</a>
                    </li>
                    <li class="">
                        <a href="https://www.bigdropinc.com/blog-categories/support-and-hosting/"
                           data-term="support-and-hosting">Support and Hosting</a>
                    </li>
                    <li class="">
                        <a href="https://www.bigdropinc.com/blog-categories/video-marketing/"
                           data-term="video-marketing">Video Marketing</a>
                    </li>
                    <li class="">
                        <a href="https://www.bigdropinc.com/blog-categories/website-design-development/"
                           data-term="website-design-development">Website Design and Development</a>
                    </li>
                </ul>
                <div class="container">
                    <div class="feed box-item">

                        <?php while (have_posts()) : the_post(); ?>
                            <?php
                            $image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');?>
                            <article class="article load-item">
                                <a class="body"
                                   href="<?php echo get_permalink(); ?>">
                                    <div class="visual"
                                         style="background-image: url('<?php echo $image_url[0]; ?>')"></div>
                                    <div class="content">
                                        <h6><?php the_title(); ?></h6>
                                        <?php
                                        $categories = get_the_category($post_id);
                                        if ($categories) {
                                            echo '';
                                            foreach ($categories as $category) {
                                                echo '<strong class="category">' . $category->cat_name . '</strong>';
                                            }
                                            echo '';
                                        }

                                        ?>

                                        <span> <?php echo get_the_date('F j  Y '); ?></span>
                                    </div>
                                </a>
                            </article>                       <?php endwhile; ?>



                    </div>

                    <?php
                    $args = array(
                        'show_all'     => false, // показаны все страницы участвующие в пагинации
                        'end_size'     => 5,     // количество страниц на концах
                        'mid_size'     => 0,     // количество страниц вокруг текущей
                        'prev_next'    => true,  // выводить ли боковые ссылки "предыдущая/следующая страница".
                        'prev_text'    => __(''),
                        'next_text'    => __(''),
                        'add_args'     => false, // Массив аргументов (переменных запроса), которые нужно добавить к ссылкам.
                        'add_fragment' => '',     // Текст который добавиться ко всем ссылкам.
                        'screen_reader_text' => __( 'Posts navigation' ),
                    );

                    the_posts_pagination($args); ?>

                </div>
            </section>
        </main>
    </div>


<?php
get_footer();
