<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="content-language" content="en-us">
    <meta name="google-site-verification" content="PE5jPVfEwvaLl4mkS5h4xnuL86snUEC-N6fuJNYnzng">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?php ?></title>

    <script>document.createElement('header');
        document.createElement('nav');
        document.createElement('section');
        document.createElement('article');
        document.createElement('aside');
        document.createElement('footer');</script>
    <meta name="description"
          content="Big Drop Inc is a Digital Agency that specializes in website design, custom development, mobile apps. Top rated NYC web design company.">
    <link rel="canonical" href="./">
    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Web Design Company in New York - Website Development | Big Drop Inc">
    <meta property="og:description"
          content="Big Drop Inc is a Digital Agency that specializes in website design, custom development, mobile apps. Top rated NYC web design company.">
    <meta property="og:url" content="https://www.bigdropinc.com/">
    <meta property="og:site_name" content="Big Drop Inc">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:description"
          content="Big Drop Inc is a Digital Agency that specializes in website design, custom development, mobile apps. Top rated NYC web design company.">
    <meta name="twitter:title" content="Web Design Company in New York - Website Development | Big Drop Inc">
    <script type="application/ld+json">
        {"@context":"https:\/\/schema.org","@type":"WebSite","@id":"#website","url":"https:\/\/www.bigdropinc.com\/","name":"Big Drop Inc","potentialAction":{"@type":"SearchAction","target":"https:\/\/www.bigdropinc.com\/?s={search_term_string}","query-input":"required name=search_term_string"}}
    </script>
    <script type="application/ld+json">
        {"@context":"https:\/\/schema.org","@type":"Organization","url":"https:\/\/www.bigdropinc.com\/","sameAs":[],"@id":"https:\/\/www.bigdropinc.com\/#organization","name":"Big Drop Inc","logo":"https:\/\/www.bigdropinc.com\/wp-content\/uploads\/2018\/04\/fknjIDpq_400x400.png"}
    </script>
    <meta name="google-site-verification" content="bhzhpyt-0jgrITaKdvbPKFa75B6o2SCp9JBVvUz8-1w">
    <link rel="dns-prefetch" href="https://tracker.gaconnector.com/">
    <link rel="dns-prefetch" href="https://s.w.org/">
    <script type="text/javascript">
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/2.4\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/2.4\/svg\/",
            "svgExt": ".svg",
            "source": {"concatemoji": "https:\/\/www.bigdropinc.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=1532022376"}
        };
        !function (a, b, c) {
            function d(a, b) {
                var c = String.fromCharCode;
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, a), 0, 0);
                var d = k.toDataURL();
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, b), 0, 0);
                var e = k.toDataURL();
                return d === e
            }

            function e(a) {
                var b;
                if (!l || !l.fillText) return !1;
                switch (l.textBaseline = "top", l.font = "600 32px Arial", a) {
                    case"flag":
                        return !(b = d([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819])) && (b = d([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]), !b);
                    case"emoji":
                        return b = d([55357, 56692, 8205, 9792, 65039], [55357, 56692, 8203, 9792, 65039]), !b
                }
                return !1
            }

            function f(a) {
                var c = b.createElement("script");
                c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
            }

            var g, h, i, j, k = b.createElement("canvas"), l = k.getContext && k.getContext("2d");
            for (j = Array("flag", "emoji"), c.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, i = 0; i < j.length; i++) c.supports[j[i]] = e(j[i]), c.supports.everything = c.supports.everything && c.supports[j[i]], "flag" !== j[i] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[j[i]]);
            c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function () {
                c.DOMReady = !0
            }, c.supports.everything || (h = function () {
                c.readyCallback()
            }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", h, !1), a.addEventListener("load", h, !1)) : (a.attachEvent("onload", h), b.attachEvent("onreadystatechange", function () {
                "complete" === b.readyState && c.readyCallback()
            })), g = c.source || {}, g.concatemoji ? f(g.concatemoji) : g.wpemoji && g.twemoji && (f(g.twemoji), f(g.wpemoji)))
        }(window, document, window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>


    <style type="text/css">.recentcomments a {
            display: inline !important;
            padding: 0 !important;
            margin: 0 !important;
        }</style>
    <style type="text/css" id="syntaxhighlighteranchor"></style>
    <style>.async-hide {
            opacity: 0 !important
        } </style>
    <style> #nav {
            margin-top: 25px;
        }

        .menu .menu-item a {
            font-family: 'Oswald', sans-serif !important;
        }
    </style>
    <script type="text/javascript">
        function myFunction() {

            document.getElementById("Location__c").value = document.location.host;
            document.getElementById("oid").value = "00DF0000000gvxy";

            $("#oid").val("00DF0000000gvxy");
            $("#Location__c").val(document.location.host);

            var fullName = $('#full_name').val().split(" ");
            var filterFullName = $.map(fullName, function (val) {
                return val === "" ? null : val;
            });

            if (fullName.length > 1) {
                $('#first_name').val(filterFullName[0]);
                $('#last_name').val(filterFullName[1]);
            } else if (fullName.length == 1) {
                $('#last_name').val(filterFullName[0]);
            }
        }

        function myFunction1() {

            document.getElementById("q_Location__c").value = document.location.host;
            document.getElementById("q_oid").value = "00DF0000000gvxy";

            $("#q_oid").val("00DF0000000gvxy");
            $("#q_Location__c").val(document.location.host);

            var fullName = $('#q_full_name').val().split(" ");
            var filterFullName = $.map(fullName, function (val) {
                return val === "" ? null : val;
            });

            if (fullName.length > 1) {
                $('#q_first_name').val(filterFullName[0]);
                $('#q_last_name').val(filterFullName[1]);
            } else if (fullName.length == 1) {
                $('#q_last_name').val(filterFullName[0]);
            }
        }
    </script>

    <script>
        var CaptchaCallback = function () {
            jQuery('.g-recaptcha').each(function (index, el) {
                var widgetId = grecaptcha.render(el, {
                    'sitekey': '6LdaQTsUAAAAAFdEmpWSw0odlHH-UiD5uQ1g3XjP',
                    'theme': 'dark'
                });
                jQuery(this).attr('data-widget-id', widgetId);
            });
        };

        function timestamp() {
            var response = document.getElementById("g-recaptcha-response");
            if (response == null || response.value.trim() == "") {
                var elems = JSON.parse(document.getElementById("captcha_settings").value);
                elems["ts"] = JSON.stringify(new Date().getTime());
                document.getElementById("captcha_settings").value = JSON.stringify(elems);
            }
        }

        function timestamp2() {
            var response = document.getElementById("g-recaptcha-response-1");
            if (response == null || response.value.trim() == "") {
                var elems = JSON.parse(document.getElementById("captcha_settings-1").value);
                elems["ts"] = JSON.stringify(new Date().getTime());
                document.getElementById("captcha_settings-1").value = JSON.stringify(elems);
            }
        }

        setInterval(timestamp2, 500);

    </script>
    <?php wp_head(); ?>
</head>
<body class="blog-template-default single single-blog postid-1554" data-controller="">

<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5ZH9TTC" height="0" width="0"
            style="display:none;visibility:hidden"></iframe>
</noscript>

<a href="https://plus.google.com/110423557470223494373" rel="publisher"></a>
<div id="page" class="site ">
    <header id="header" class="">
        <div class="row header">
            <a id="logo" href="<?php echo get_home_url(); ?>" rel="home"></a>
            <?php wp_nav_menu(array(
                'theme_location' => 'main',
                'menu' => 'main',
                'container' => 'nav',
                'container_class' => 'main-navigation',
                'container_id' => 'nav',
                'menu_class' => 'menu',
                'menu_id' => 'menu-main',
                'echo' => true,
                'fallback_cb' => 'wp_page_menu',
                'before' => '',
                'after' => '',
                'link_before' => '',
                'link_after' => '',
                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'depth' => 0,
                'walker' => '',
            )); ?>
            <a class="forkit" data-text="Užpildyti užklausą" data-text-detached="Traukti žemyn &gt;"></a>
            <style type="text/css">#nav {
                    margin-right: 100px
                }</style>
            <a id="m-nav" href="#">Menu <span class="fa">&#xf039;</span></a>
            <div class="clear"></div>
        </div>
    </header>