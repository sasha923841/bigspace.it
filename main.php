<?php
/*
Template Name: Main page
*/

get_header();
?>

    <div id="content" class="home-page-content">
        <ul id="navi">
            <li><a href="#slider" class="active"></a></li>
            <li><a href="#digital-agency"></a></li>
            <li><a href="#trends"></a></li>
            <li><a href="#identity"></a></li>
            <li><a href="#personal"></a></li>
        </ul>
        <section id="slider" class="side side-1">
            <div id="home-slider-wrap" class="home-slider">
                <div class="item about " style="background-image:url( <?php echo get_template_directory_uri() .  '/assets/images/desktop.jpg'?>);">
                    <div class="item-wrap">
                        <div class="item-text-wrap">
                            <?php while( have_posts() ) : the_post();

                                the_content(); // выводим контент
                            endwhile; ?>
                            <a href="<?php echo carbon_get_post_meta(get_the_ID(), 'crb_portfolio_url') ; ?>" class="bd-button" style="font-family: 'Oswald', sans-serif;"><?php echo carbon_get_post_meta(get_the_ID(), 'crb_text') ; ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <a id="subnav" href="#digital-agency" class="trigger"><img src="<?php echo get_template_directory_uri() .  '/assets/scroll1.png'?>" alt="scroll"></a>
        </section>
        <section id="digital-agency" class="side">
            <div class="item-wrap">
                <div class="content-text">
                    <p style="font-family: 'Oswald', sans-serif; font-weight: bold;"> Turi <span>idėją</span>, bet
                        nežinai kaip ją įgyvendinti? O gal tavo įmonei jau seniai reikėjo atnaujinti savo įvaizdį
                        internetinėje erdvėje? <span>Profesionalus</span> interneto svetainių kūrimas, jų <span>atnaujinimas</span>
                        ir <span>palaikymas </span>. </P>

                </div>
            </div>
        </section>
        <section id="trends" class="side side-2">
            <div class="item-wrap"
                 style="background: url(https://www.bigdropinc.com/wp-content/themes/bd/images/home/pattern.png)">
                <div class="item-text">
                    <div class="text"><br>Kūrybiškumas</div>
                    <div class="picture">
                        <img class="pic animated" src="<?php echo get_template_directory_uri() .  '/assets/kurybiskumas.png'?>" alt="Web Design Company" width="605"
                             height="430">
                    </div>
                </div>
            </div>
        </section>
        <section id="identity" class="side side-3">
            <div class="item-wrap"
                 style="background: url(https://www.bigdropinc.com/wp-content/themes/bd/images/home/pattern.png)">
                <div class="item-text">
                    <div class="text">Mes suprantame savo <br>klientus</div>
                    <div class="picture">
                        <img class="pic animated" src="<?php echo get_template_directory_uri() .  '/assets/shaking2.png'?>" alt="Website Development Firm" width="687"
                             height="510">
                    </div>
                </div>
            </div>
        </section>
        <section id="personal" class="side side-4">
            <div class="item-wrap"
                 style="background: url(https://www.bigdropinc.com/wp-content/themes/bd/images/home/pattern.png)">
                <div class="item-text">
                    <div class="text">Greitas Rezultatas<br></div>
                    <div class="picture">
                        <img class="pic animated" src="<?php echo get_template_directory_uri() .  '/assets/rezultatas.png'?>" alt="Web Design Agency" width="477"
                             height="402">
                    </div>
                </div>
            </div>
        </section>
    </div>


<?php
get_footer();