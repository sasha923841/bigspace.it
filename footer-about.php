</div>
<iframe src="./assets/portal.html" id="st_gdpr_iframe"
        style="width: 0px; height: 0px; position: absolute; left: -5000px;"></iframe>
<div class="clear"></div>
<?php get_template_part( 'part', 'footer' ); ?>

<script type="text/javascript">
    /* <![CDATA[ */
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/www.bigdropinc.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }, "recaptcha": {"messages": {"empty": "Please verify that you are not a robot."}}
    };
    /* ]]> */
</script>
<script type="text/javascript">
    /* <![CDATA[ */
    var pollsL10n = {
        "ajax_url": "https:\/\/www.bigdropinc.com\/wp-admin\/admin-ajax.php",
        "text_wait": "Your last request is still being processed. Please wait a while ...",
        "text_valid": "Please choose a valid poll answer.",
        "text_multiple": "Maximum number of choices allowed: ",
        "show_loading": "1",
        "show_fading": "1"
    };
    /* ]]> */
</script>
<script type="text/javascript">
    /* <![CDATA[ */
    var myAjax = {"ajaxurl": "https:\/\/www.bigdropinc.com\/wp-admin\/admin-ajax.php"};
    /* ]]> */
</script>
<img src=" <?php echo get_template_directory_uri() .  './assets/who.ashx' ?>"style="display: none;">
<iframe name="oauth2relay415009696" id="oauth2relay415009696" src="<?php echo get_template_directory_uri() . '/assets/postmessageRelay.html'?>" tabindex="-1"
        aria-hidden="true" style="width: 1px; height: 1px; position: absolute; top: -100px;"></iframe>


<style>
    .g-recaptcha > div {
        margin: 0 auto;
    }
</style>
<script>
    var main_form = $('form.main_form, form.footer_form');
    main_form.on('submit', function (e) {
        var serialize_form = $(this).serialize();
        var _this = $(this);
        var data = {
            'action': 'submit_form',
            'data': serialize_form
        };
        $.ajax({
            url: '/wp-admin/admin-ajax.php',
            data: data,
            type: 'POST',
            beforeSend: function () {
                _this.find('input[type="submit"]').attr('disabled', 'disabled');
            },
            success: function (res) {
                if (res.status) {
                    _this.find('.form-error').text(res.msg).show();
                    setTimeout(function () {
                        document.location.href = res.redirect_url;
                    }, 1500);
                    _this.find('input[type="submit"]').removeAttr('disabled', 'disabled');
                } else {
                    _this.find('.form-error').text(res.msg).show().fadeOut(4000);
                    _this.find('input[type="submit"]').removeAttr('disabled', 'disabled');
                }
                return false;
            }
        });
        return false;
    });
</script>
<script>
    var new_form = $(document).find('form#new-request-form');
    new_form.on('click', '.submit-step', function (e) {
        e.preventDefault();
        myFunction1();
        var serialize_form = new_form.serialize();
        var data = {
            'action': 'submit_new_form',
            'data': serialize_form
        };
        $.ajax({
            url: '/wp-admin/admin-ajax.php',
            data: data,
            type: 'POST',
            beforeSend: function () {
            },
            success: function (res) {
                switch (res.status) {
                    case 'validate_error':
                        new_form.find('.form-error').text(res.msg).show().fadeOut(4000);
                        var names = [];
                        $.each(res.errors, function (key, value) {
                            names.push(value);
                        });
                        if (names.length > 0) {
                            new_form.find('.parsley-error').each(function () {
                                $(this).removeClass('parsley-error');
                            });
                            $.each(names, function (index, value) {
                                new_form.find("[name='" + value + "']").addClass('parsley-error');
                            });
                        }
                        break;
                    case 'captcha_error':
                        new_form.find('.form-error').text(res.msg).show().fadeOut(4000);
                        break;
                    case 'step1_is_valid':
                        new_form.find('.parsley-error').each(function () {
                            $(this).removeClass('parsley-error');
                        });
                        new_form.find('#step1').hide();
                        new_form.find('#step1').after(res.html);
                        $("#range").ionRangeSlider({
                            hide_min_max: true,
                            hide_from_to: true,
                            grid: false,
                            values: rangeOpts.values,
                            from: rangeOpts.from,
                            onStart: function (data) {
                                var val = data.from_value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                $('.range-result span').text(val);
                            },
                            onChange: function (data) {
                                var val = data.from_value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                $('.range-result span').text(val);
                            },
                        });
                        break;
                    case 'success':
                        new_form.find('.parsley-error').each(function () {
                            $(this).removeClass('parsley-error');
                        });
                        new_form.find('.form-error').text(res.msg).show();
                        setTimeout(function () {
                            document.location.href = res.redirect_url;
                        }, 1500);
                        break;
                }
                return false;
            }
        });
        return false;
    });

    new_form.on('click', '.prev-step', function (e) {
        e.preventDefault();
        var current_fieldset = $(this).closest('fieldset');
        current_fieldset.remove();
        grecaptcha.reset();
        new_form.find('fieldset#step1').show();
    });
</script>

<div id="drift-widget-container" style="position: absolute; z-index: 2147483647;">
    <iframe id="drift-widget" src="./assets/index.html" title="Drift Messenger" role="complementary"
            class="drift-widget-hidden"
            style="border: none; display: block; height: 0px !important; width: 0px !important; position: fixed; top: auto; left: auto; bottom: 0px; right: 0px; visibility: hidden; z-index: 2147483647; max-height: 100vh; max-width: 100vw; transition: none 0s ease 0s; background: none transparent; opacity: 1;"></iframe>
</div>

<?php wp_footer(); ?>

</body>
</html>