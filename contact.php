<?php
/*
Template Name: Contact page
*/

get_header('contact');
?>
    <div id="content" class="" data-ce-key="139">
        <div id="contact-us" data-ce-key="140">
            <div class="contact-us-wrap"
                 style="background-image: url(https://www.bigdropinc.com/wp-content/uploads/2018/07/contact-bg.jpg)"
                 data-ce-key="141">
                <div class="contact-us-title" data-ce-key="142">
                    <div class="container" data-ce-key="143">
                        <?php while( have_posts() ) : the_post();

                            the_content(); // выводим контент
                        endwhile; ?>

                        <a href="#" class="btn btn-white request-quote btn-request-quote" style="border:none;">Užpildyti užklausą</a>
                    </div>
                </div>
                <script> $("#Uzklausa").click(function () {
                        alert("cia");
                    }); </script>


                <div class="container" data-ce-key="147">
                    <h1 data-ce-key="148"><?php echo carbon_get_post_meta(get_the_ID(), 'crb_2_title') ; ?></h1>
                    <div class="contact-us-info" data-ce-key="149">
                        <div class="contact-list" data-ce-key="150">
                            <div class="item" data-ce-key="151">
                                <?php echo wpautop(carbon_get_post_meta(get_the_ID(), 'crb_text1')); ?>
                            </div>
                            <div class="item" data-ce-key="157">
                                <?php echo wpautop(carbon_get_post_meta(get_the_ID(), 'crb_text2')); ?>

                            </div>
                            <div class="item" data-ce-key="163">
                                <?php echo wpautop(carbon_get_post_meta(get_the_ID(), 'crb_text3')); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="new-location-boxes" data-ce-key="173">
                    <div class="container" data-ce-key="174">
                        <div class="heading" data-ce-key="175">
                            <h2 class="h1" data-ce-key="176"><?php echo carbon_get_post_meta(get_the_ID(), 'crb_3_title') ; ?></h2>
                        </div>
                        <div class="new-location-list" data-ce-key="177">
                            <div class="item" data-ce-key="178">
                                <?php echo wpautop(carbon_get_post_meta(get_the_ID(), 'crb_text11')); ?>

                            </div>
                            <div class="item" data-ce-key="183">
                                <?php echo wpautop(carbon_get_post_meta(get_the_ID(), 'crb_text22')); ?>

                            </div>
                            <div class="item" data-ce-key="188">
                                <?php echo wpautop(carbon_get_post_meta(get_the_ID(), 'crb_text33')); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer('sub-services'); ?>
