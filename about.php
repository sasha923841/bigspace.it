<?php
/*
Template Name: About page
*/

get_header('about');
?>
    <div id="content" class="">
        <div id="service" class="about" style="height: 626px;">
            <div class="service-item parallax"
                 style="background-image: url(<?php echo get_template_directory_uri() . '/assets/background-frame.jpg' ?>); background-position: 50% 0px; height: 626px;">
                <div class="service-item-text" style="margin-top: -72px; top: 50%;">
                    <div class="text" style="top: 0px; opacity: 1;">
                        <?php while (have_posts()) : the_post();

                            the_content(); // выводим контент
                        endwhile; ?>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <section id="about">
            <section class="about about1">
                <article class="about-content">
                    <div class="layout">
                        <div class="subtitle"><?php echo carbon_get_post_meta(get_the_ID(), 'crb_first_text') ; ?></div>
                    </div>
                </article>
                <style> .about1 .about-content .layout .subtitle {
                        padding-top: 200px;
                    } </style>
                <aside class="thumb">
                    <div class="layout">
                        <img src="<?php echo carbon_get_post_meta(get_the_ID(), 'photo1');?>"
                             alt="best web development firm">
                    </div>
                </aside>
            </section>
            <section class="about about2">
                <article class="about-content">
                    <div class="layout">
                        <div class="subtitle"><?php echo carbon_get_post_meta(get_the_ID(), 'crb_second_text') ; ?>
                        </div>
                    </div>
                </article>
                <style> .about2 .about-content .layout .subtitle {
                        padding-top: 70px;
                    } </style>
                <aside class="thumb">
                    <div class="layout" >
                        <img style="width:90%;" src="<?php echo carbon_get_post_meta(get_the_ID(), 'photo2');?>"
                             alt="web design company">
                    </div>
                </aside>
            </section>
            <section class="about about3">
                <article class="about-content">
                    <div class="layout">
                        <div class="subtitle"><?php echo carbon_get_post_meta(get_the_ID(), 'crb_third_text') ; ?>
                        </div>
                    </div>
                </article>
                <aside class="thumb">
                    <div class="layout">
                        <img src="<?php echo carbon_get_post_meta(get_the_ID(), 'photo3');?>"
                             alt="best web development firm">
                    </div>
                </aside>
            </section>
            <section class="about about2">
                <article class="about-content">
                    <div class="layout">
                        <div class="subtitle"> <?php echo carbon_get_post_meta(get_the_ID(), 'crb_fourth_text') ; ?>
                        </div>
                    </div>
                </article>
                <aside class="thumb">
                    <div class="layout">
                        <img src="<?php echo carbon_get_post_meta(get_the_ID(), 'photo4');?>"
                             alt="web design company">
                    </div>
                </aside>
            </section>
            <style> .about-content .layout .subtitle {
                    font-family: 'Open Sans', sans-serif;
                } </style>
            <section class="about about2">
                <article class="about-content">
                    <div class="layout">
                        <div class="subtitle">
                            <br><br><a href="<?php echo carbon_get_post_meta(get_the_ID(), 'crb_su_url') ; ?>">Susisiekite &gt; </a>
                        </div>
                    </div>
                </article>
            </section>
        </section>
    </div>

<?php
get_footer();
