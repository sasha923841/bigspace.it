<?php

get_header();
?>
    <div id="content" class="">
    <main id="blogArchive" class="single-post">
<?php
while (have_posts()) :
    the_post(); ?>
    <section class="post-title">
        <div class="container">
            <div class="frame animated">
                <?php
                $categories = get_the_category($post_id);
                if ($categories) {
                    echo '';
                    foreach ($categories as $category) {
                        echo '<strong class="category">' . $category->cat_name . '</strong>';
                    }
                    echo '';
                }

                ?>
                <h1><?php the_title(); ?></h1>
                <div class="add-info">
                    <div class="published">
                        Published on
                        <time><?php echo get_the_date('F j  Y '); ?></time>
                    </div>
                    <div class="author">
                        Words by
                        <strong><?php the_author(); ?></strong>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
        <div class="image-holder animated"
             style="background-image: url('<?php echo $image_url[0]; ?>')"></div>
    </section>
    <section class="wrap">
    <div class="container _row">
        <div class="body">
            <div class="share-left has-animate">
                <ul class="social" id="social">
                    <li>
                        <a href="javascript:void(0)" data-network="facebook" class="st-custom-button"
                           data-url="https://www.bigdropinc.com/blog/5-ways-to-make-sure-that-your-website-meets-ada-compliance/"
                           data-title="5 Ways to Make Sure that Your Website Meets ADA Compliance"
                           data-description="ADA website compliance is a tricky subject that can throw many web designers and developers for a loop. The Americans with Disabilities Act mandates that all public spaces, including some websites, b..."
                           data-image="https://www.bigdropinc.com/wp-content/uploads/2018/03/Blog-header_WEBSITE_ADA.png">
                            <img src="https://www.bigdropinc.com/wp-content/themes/bd/images/ico-facebook-black1.svg"
                                 alt="ico">
                            <span style="background-image: url('https://www.bigdropinc.com/wp-content/themes/bd/images/ico-facebook-black1.svg')"></span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" data-network="pinterest" class="st-custom-button"
                           data-url="https://www.bigdropinc.com/blog/5-ways-to-make-sure-that-your-website-meets-ada-compliance/"
                           data-description="ADA website compliance is a tricky subject that can throw many web designers and developers for a loop. The Americans with Disabilities Act mandates that all public spaces, including some websites, b..."
                           data-image="https://www.bigdropinc.com/wp-content/uploads/2018/03/Blog-header_WEBSITE_ADA.png">
                            <img src="https://www.bigdropinc.com/wp-content/themes/bd/images/ico-pinterest-black1.svg"
                                 alt="ico">
                            <span style="background-image: url('https://www.bigdropinc.com/wp-content/themes/bd/images/ico-pinterest-black1.svg')"></span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" data-network="twitter" class="st-custom-button"
                           data-url="https://www.bigdropinc.com/blog/5-ways-to-make-sure-that-your-website-meets-ada-compliance/"
                           data-description="5 Ways to Make Sure that Your Website Meets ADA Compliance"
                           data-via="BigDropInc">
                            <img src="https://www.bigdropinc.com/wp-content/themes/bd/images/ico-twitter-black1.svg"
                                 alt="ico">
                            <span style="background-image: url('https://www.bigdropinc.com/wp-content/themes/bd/images/ico-twitter-black1.svg')"></span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" data-network="linkedin" class="st-custom-button"
                           data-url="https://www.bigdropinc.com/blog/5-ways-to-make-sure-that-your-website-meets-ada-compliance/"
                           data-title="5 Ways to Make Sure that Your Website Meets ADA Compliance"
                           data-description="ADA website compliance is a tricky subject that can throw many web designers and developers for a loop. The Americans with Disabilities Act mandates that all public spaces, including some websites, b..."
                           data-via="BigDropInc"
                           data-image="https://www.bigdropinc.com/wp-content/uploads/2018/03/Blog-header_WEBSITE_ADA.png">
                            <img src="https://www.bigdropinc.com/wp-content/themes/bd/images/linkedin1.svg"
                                 alt="ico">
                            <span style="background-image: url('https://www.bigdropinc.com/wp-content/themes/bd/images/linkedin1.svg')"></span>
                        </a>
                    </li>
                    <li>
                        <a class="views"
                           href="/cdn-cgi/l/email-protection#3d024e485f57585e490008180f0d6a5c444e180f0d4952180f0d705c5658180f0d6e484f58180f0d49555c49180f0d6452484f180f0d6a585f4e544958180f0d705858494e180f0d7c797c180f0d7e52504d51545c535e581b5f525944005549494d4e0712124a4a4a135f545a594f524d54535e135e5250125f51525a1208104a5c444e10495210505c5658104e484f581049555c49104452484f104a585f4e54495810505858494e105c595c105e52504d51545c535e5812">
                            <img src="https://www.bigdropinc.com/wp-content/themes/bd/images/ico-email-black1.svg"
                                 alt="ico">
                            <span style="background-image: url('https://www.bigdropinc.com/wp-content/themes/bd/images/ico-email-black1.svg')"></span>
                        </a>
                    </li>
                </ul>
                <strong>Share</strong>
            </div>
        </div>
        <div class="body">
            <form>
                <input type="button" value="Назад на предыдущую страницу"
                       onClick="history.back()">
            </form>
            <a onClick="history.back()" class="back-link">< Back to main blog</a>
            <?php the_content(); ?>

        </div>
        <div class="request-block text-center">
            <div class="container">
                <h2 class="h1">Let's create something together.</h2>
                <a href="#" class="btn btn-white request-quote btn-request-quote">Request a Quote</a>
            </div>
        </div>
        <div class="container">
            <h2 class="text-center">You might also like</h2>
            <ul class="info-list big">
                <li class="animated">
<span class="image-holder">
<a href="https://www.bigdropinc.com/blog/risks-non-ada-compliant-website/">link</a>
<span class="image"
      style="background-image: url('https://www.bigdropinc.com/wp-content/uploads/2018/02/All-the-Possible-Risks-of-Not-Having-an-ADA-Compliant-Website_2v-1024x576.png')">image</span>
</span>
                    <h3><a href="https://www.bigdropinc.com/blog/risks-non-ada-compliant-website/">All the
                            Possible Risks of Not Having an ADA Compliant Website</a></h3>
                    <strong class="category">Support and Hosting</strong>
                    <strong class="category">Website Design and Development</strong>
                    <time>Published on May 25th, 2018</time>
                </li>
                <li class="animated">
<span class="image-holder">
<a href="https://www.bigdropinc.com/blog/ada-compliance-for-website/">link</a>
<span class="image"
      style="background-image: url('https://www.bigdropinc.com/wp-content/uploads/2018/02/What-is-ADA-Compliance-for-Websites_-01-1024x571.png')">image</span>
</span>
                    <h3><a href="https://www.bigdropinc.com/blog/ada-compliance-for-website/">What is ADA
                            Compliance for Websites</a></h3>
                    <strong class="category">Support and Hosting</strong>
                    <strong class="category">Website Design and Development</strong>
                    <time>Published on March 18th, 2018</time>
                </li>
                <li class="animated">
<span class="image-holder">
<a href="https://www.bigdropinc.com/blog/how-to-back-up-your-website-and-why-its-important/">link</a>
<span class="image"
      style="background-image: url('https://www.bigdropinc.com/wp-content/uploads/2018/03/Is-Voice-Marketing-the-Next-Big-Thing-for-Marketing_-02-1024x576.png')">image</span>
</span>
                    <h3>
                        <a href="https://www.bigdropinc.com/blog/how-to-back-up-your-website-and-why-its-important/">How
                            to Back up your Website and Why It’s Important</a></h3>
                    <strong class="category">Support and Hosting</strong>
                    <strong class="category">Website Design and Development</strong>
                    <time>Published on March 15th, 2018</time>
                </li>
            </ul>
        </div>
    </div>

<?php endwhile; // End of the loop.
?>



<?php
get_footer();
