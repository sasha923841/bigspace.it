<footer id="footer">
    <ul class="footer social">
        <li><a target="_blank" href="<?php echo carbon_get_theme_option('crb_facebook_url'); ?>"
               class="fa fb"><span></span></a></li>
        <li><a target="_blank" href="<?php echo carbon_get_theme_option('crb_twwiter_url'); ?>" class="fa tw"></a></li>
        <li><a target="_blank" href="<?php echo carbon_get_theme_option('crb_in_url'); ?>" class="fa linkedin"></a>
        </li>
        <li><a target="_blank" href="<?php echo carbon_get_theme_option('crb_google_url'); ?>" class="fa gp"></a>
        </li>
        <li><a target="_blank" href="<?php echo carbon_get_theme_option('crb_instagram_url'); ?>" class="fa in"></a>
        </li>
        <li><a target="_blank" href="<?php echo carbon_get_theme_option('crb_pinterest_url'); ?>" class="fa pt"></a>
        </li>
        <li><a target="_blank" href="<?php echo carbon_get_theme_option('crb_beh_url'); ?>" class="fa behance"></a>
        </li>
    </ul>
    <div class="bd-footer">
        Big Drop Inc is a premier web design and web development company with offices in New York, Los Angeles and
        Miami<span class="sep"></span>
        <div itemscope="" >
            <span itemprop="name">Big Drop Inc</span> has been rated in Google+
            <span itemprop="aggregateRating" itemscope="" >
                     <?php echo wpautop(carbon_get_theme_option('crb_footer_text'));?>

                     <a href="<?php echo carbon_get_theme_option('crb_email_protected'); ?>"><span
                                 class="__cf_email__" >[email&nbsp;protected]</span></a>
                     <ul>
                        <li><a target="_blank" href="<?php echo carbon_get_theme_option('crb_privacy_policy_url'); ?>">Privacy Policy</a></li>
                        <li><a target="_blank"
                               href="<?php echo carbon_get_theme_option('crb_terms-conditions_url'); ?>">Terms and Conditions</a></li>
                     </ul>
                  </span>
        </div>
    </div>
</footer>
<div id="request_form" class="new-request-form">
    <div class="close-button"><span class="fa"></span></div>
    <div class="form-holder">
        <div class="form-content container">
            <div class="title">Start a Project</div>
            <div class="subtitle">Thanks for your interest in working with us. Please complete the details below and
                we’ll get back to you within one business day.
            </div>
            <form action="" id="new-request-form">
                <div class="required-fields">
                    All fields are required
                </div>
                <fieldset id="step1">
                    <input type="hidden" id="q_oid" class="oid" name="oid" value="00DF0000000gvxy">
                    <input type="hidden" id="q_Location__c" class="Location__c" name="Location__c" value="">
                    <input type="hidden" name="retURL" value="https://bigdropinc.com/thank-you/">
                    <input type="hidden" name="lead_source" value="Request a Quote">
                    <input type="hidden" name="Referral_URL__c" value="">
                    <input type="hidden" id="gclid" name="00N2A00000DSpCt" value="">
                    <input type="hidden" id="captcha_settings-1" name="captcha_settings"
                           value="{&quot;keyname&quot;:&quot;reCaptcha&quot;,&quot;fallback&quot;:&quot;true&quot;,&quot;orgId&quot;:&quot;00DF0000000gvxy&quot;,&quot;ts&quot;:&quot;&quot;}">
                    <input class="gaconnector_fc_source" name="00N2A00000D9heS" type="hidden" value=""><input
                            class="gaconnector_lc_source" name="00N2A00000D9heE" type="hidden" value=""><input
                            class="gaconnector_fc_medium" name="00N2A00000D9heR" type="hidden" value=""><input
                            class="gaconnector_lc_medium" name="00N2A00000D9heC" type="hidden" value=""><input
                            class="gaconnector_fc_campaign" name="00N2A00000D9heP" type="hidden" value=""><input
                            class="gaconnector_lc_campaign" name="00N2A00000D9he8" type="hidden" value=""><input
                            class="gaconnector_fc_term" name="00N2A00000D9heT" type="hidden" value=""><input
                            class="gaconnector_lc_term" name="00N2A00000D9heF" type="hidden" value=""><input
                            class="gaconnector_fc_content" name="00N2A00000D9heQ" type="hidden" value=""><input
                            class="gaconnector_lc_content" name="00N2A00000D9heA" type="hidden" value=""><input
                            class="gaconnector_fc_landing" name="00N2A00000D9he4" type="hidden" value=""><input
                            class="gaconnector_lc_landing" name="00N2A00000D9heB" type="hidden" value=""><input
                            class="gaconnector_fc_referrer" name="00N2A00000D9he5" type="hidden" value=""><input
                            class="gaconnector_lc_referrer" name="00N2A00000D9heD" type="hidden" value=""><input
                            class="gaconnector_fc_channel" name="00N2A00000D9he3" type="hidden" value=""><input
                            class="gaconnector_lc_channel" name="00N2A00000D9he9" type="hidden" value=""><input
                            class="gaconnector_OS" name="00N2A00000D9heK" type="hidden" value=""><input
                            class="gaconnector_device" name="00N2A00000D9he2" type="hidden" value=""><input
                            class="gaconnector_browser" name="00N2A00000D9hdy" type="hidden" value=""><input
                            class="gaconnector_city" name="00N2A00000D9he0" type="hidden" value=""><input
                            class="gaconnector_country" name="00N2A00000D9he1" type="hidden" value=""><input
                            class="gaconnector_time_zone" name="00N2A00000D9heO" type="hidden" value=""><input
                            class="gaconnector_pages_visited_list" name="00N2A00000D9heL" type="hidden"
                            value=""><input
                            class="gaconnector_page_visits" name="00N2A00000D9heJ" type="hidden" value=""><input
                            class="gaconnector_ip_address" name="00N2A00000D9he7" type="hidden" value=""><input
                            class="gaconnector_time_passed" name="00N2A00000D9heM" type="hidden" value=""><input
                            class="gaconnector_GA_Client_ID" name="00N2A00000D9hdz" type="hidden" value=""><input
                            class="gaconnector_latitude" name="00N2A00000D9heG" type="hidden" value=""><input
                            class="gaconnector_longitude" name="00N2A00000D9heI" type="hidden" value=""> <input
                            type="hidden" id="stepLocator" name="step" value="1">
                    <div class="form-group">
                        <label for="q_full_name">Your Name</label>
                        <input id="q_full_name" maxlength="40" class="form-control" name="full_name" size="20"
                               type="text" required="">
                        <input id="q_first_name" maxlength="40" name="first_name" size="20" type="hidden">
                        <input id="q_last_name" maxlength="80" name="last_name" size="20" type="hidden">
                    </div>
                    <div class="form-group">
                        <label for="q_email">Email</label>
                        <input type="email" id="q_email" class="form-control" name="email" required="">
                    </div>
                    <div class="form-group">
                        <label for="q_phone">Phone Number</label>
                        <input type="text" id="q_phone" class="form-control" name="phone" required="">
                    </div>
                    <div class="form-group">
                        <label for="q_company">Company Name</label>
                        <input type="text" id="q_company" class="form-control" name="company" required="">
                    </div>
                    <div class="form-group">
                        <label for="g-recaptcha"></label>
                        <div class="g-recaptcha" id="g-recaptcha"
                             data-sitekey="6LchyBUUAAAAAOULMoeReakaIuf4lXIB9EJ4VC_J"></div>
                    </div>
                    <div class="buttons-group">
                        <button class="btn form-main-btn submit-step">Next <img
                                    src="<?php echo get_template_directory_uri() . '/assets/002-next.svg' ?>"
                                    alt="Icon"></button>
                    </div>
                </fieldset>
                <fieldset id="step2" style="display:none;">
                    <div class="form-group top-label">
                        <label>Project Type</label>
                        <div class="inline-block">
                            <div>
								<span>
								<input type="checkbox" class="checkbox" name="redesigning" id="q_00NF000000DWKY9"
                                       value="1">
								<label for="q_00NF000000DWKY9"
                                       class="small custom-checkbox">Redesigning my website</label>
								</span>
                                <span>
								<input type="checkbox" class="checkbox" name="new_website" id="q_00NF000000DWKYE"
                                       value="1">
								<label for="q_00NF000000DWKYE" class="small custom-checkbox">A brand new website</label>
								</span>
                                <span>
								<input type="checkbox" class="checkbox" name="branding" id="q00NF000000DWKYT" value="1">
								<label for="q00NF000000DWKYT" class="small custom-checkbox">Branding</label>
								</span>
                                <span>
								<input type="checkbox" class="checkbox" name="marketing" id="q_00NF000000DWKYY"
                                       value="1">
								<label for="q_00NF000000DWKYY" class="small custom-checkbox">Marketing</label>
								</span>
                            </div>
                            <div>
								<span>
								<input type="checkbox" class="checkbox" name="ecommerce" id="q_00NF000000DWKYJ"
                                       value="1">
								<label for="q_00NF000000DWKYJ" class="small custom-checkbox">Ecommerce</label>
								</span>
                                <span>
								<input type="checkbox" class="checkbox" name="mobile_apps" id="q_00NF000000DWKYO"
                                       value="1">
								<label for="q_00NF000000DWKYO" class="small custom-checkbox">Mobile application</label>
								</span>
                                <span>
								<input type="checkbox" class="checkbox" name="support" id="q_00NF000000DWKYd" value="1">
								<label for="q_00NF000000DWKYd" class="small custom-checkbox">Support</label>
								</span>
                                <span>
								<input type="checkbox" class="checkbox" name="other" id="q_00NF000000DWKYi" value="1">
								<label for="q_00NF000000DWKYi" class="small custom-checkbox">Other</label>
								</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group top-label">
                        <label for="q_description">Describe the Project</label>
                        <textarea name="description" id="q_description" class="form-control" required=""></textarea>
                    </div>
                    <div class="form-group">
                        <label for="range">Estimated Budget</label>
                        <div class="range">
                            <div class="range-result">up to $<span>20,000</span></div>
                            <input id="range" name="00NF000000DWIci" type="text" value="20000"
                                   class="irs-hidden-input">
                        </div>
                    </div>
                    <div class="buttons-group inline-buttons">
                        <button class="btn form-inner-btn prev-step"><img
                                    src="https://www.bigdropinc.com/wp-content/themes/bd/images/001-back.svg"
                                    alt="Icon"></button>
                        <button type="submit" class="btn form-main-btn submit-step">Submit Your Request <img
                                    src="https://www.bigdropinc.com/wp-content/themes/bd/images/002-next.svg"
                                    alt="Icon"></button>
                    </div>
                </fieldset>
                <div style="text-align: center; color: #f1c40f;" class="form-error"></div>
            </form>
        </div>
    </div>
</div>